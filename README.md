# EGLL - London Heathrow Airport

**[Official ZDP Discord](https://discord.gg/YADGVW8zWv)**

**Overview**

This scenery is currently in a BETA state. Features will be missing and bugs will be present. The scenery is also open source, if you wish to contribute you can read the instructions located in the wiki [here](https://gitlab.com/StableSystem/egll-heathrow/-/wikis/How-to-Contribute)

**Installation Instructions:**

Download and unzip the egll-heathrow folder into your "Custom Scenery" folder

It is recommended to use x-organizer to ensure correct scenery load order. This may also be done manually by opening the scenery_packs.ini file in the "Custom Scenery" folder, and moving "egll-heathrow" to the top of the list. 

**Required Libraries**

* ZDP Library v1.1+

* MisterX Library v2.0+

* SAM Library v3+

Please report any bugs on the official ZDP discord (linked above)

To contact us about this repo please send a message on Discord @StableSystem

This repository and it's contents are protected under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)
Assets used from other developers were done so with their knowledge and approval.

**Credits**

ZDP Developers: StableSystem, TJ, Martini, Awfsiu, imran, Solrikt

Ortho source - Ortho obtained from Blue Sky Network with commercial usage rights. Contact a member of the ZDP staff for more information. 

Thanks to Jan Vogel for creating the EGLL gateway scenery which was used as a foundation for this scenery. 

**Open Source Resources**

EGLL source - https://gitlab.com/StableSystem/egll-heathrow
